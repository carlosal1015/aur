```console
mkdir ~/.ssh
echo $SSH_PUBLIC_KEY > ~/.ssh/id_ed25519.pub
chmod 644 ~/.ssh/id_ed25519.pub
echo -e $SSH_PRIVATE_KEY > ~/.ssh/id_ed25519
chmod 600 ~/.ssh/id_ed25519
ssh aur@aur.archlinux.org help
# https://stackoverflow.com/a/56163560
# https://github.com/gitpod-io/gitpod/issues/666#issuecomment-534124994
# https://www.reliableembeddedsystems.com/wiki/index.php?title=Gitpod
# ssh-add - <<< "${PRIVATE_SSH_KEY}"
# echo $SSH_PRIVATE_KEY | tr -d '\r' | ssh-add - > /dev/null
# ssh-add -K ~/.ssh/id_ed25519
```
<!-- awk '{printf "%s\\n", $0}' -->
```console
mkdir ~/build
cd ~/build
curl -LO https://aur.archlinux.org/cgit/aur.git/snapshot/aur-out-of-date.tar.gz
tar -xvf aur-out-of-date.tar.gz
cd aur-out-of-date
makepkg --noconfirm -si
rm -rf ~/.cache
sudo rm -rf ~/go
rm -rf ~/build
```

```console
ssh://aur@aur.archlinux.org/foo.git
docker run -u aur -it --rm registry.gitlab.com/dune-archiso/images/dune-archiso # {dune-archiso,dune-archiso-arch4edu,dune-archiso-yay,dune-archiso-paru}
yay -G foo
cd foo
makepkg --allsource
makepkg --printsrcinfo > .SRCINFO
sha512sums // xsel, xclip
```

```
gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 5BC6FBBAB02C73E4724B2CFC8C43C00BA8F06ECA
gpg: key 8C43C00BA8F06ECA: public key "Carlos Aznarán <caznaranl@uni.pe>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg -k
/home/gitpod/.gnupg/pubring.kbx
-------------------------------
pub   rsa4096 2021-04-04 [SC] [expires: 2023-01-13]
      5BC6FBBAB02C73E4724B2CFC8C43C00BA8F06ECA
uid           [ unknown] Carlos Aznarán <caznaranl@uni.pe>
sub   rsa4096 2021-04-04 [E] [expires: 2022-04-04]
```