## Description

Daily building from AUR packages maintained by carlosal1015. [Dune modules](https://aur.archlinux.org/packages/dune-core) and [preCICE adapters](https://aur.archlinux.org/packages?&K=precice) have a special weekly building [here](https://gitlab.com/dune-archiso/testing/aur) and [here](https://gitlab.com/dune-archiso/testing/precice-arch), respectively.

## Feedback

Feel free to leave a message using [Etherpad](https://yopad.eu/p/AUR_report-365days) (no login required) related to suggestion, issues, etc.

## Broken packages

- `freewrl`

  because [freewrl](https://archlinux.org/todo/remove-js185-from-the-repos) **was deleted from official repository**. _Required for [`octave-vrml`](https://aur.archlinux.org/packages/octave-vrml)_.

- [`tetgen`](https://aur.archlinux.org/packages/tetgen) [![AUR](https://img.shields.io/aur/votes/tetgen.svg)](https://aur.archlinux.org/packages/tetgen)

  because **the source URL response 404 status code**. _Required for [`mshr-git`](https://aur.archlinux.org/packages/mshr-git)_.

- [`mshr-git`](https://aur.archlinux.org/packages/mshr-git) [![AUR](https://img.shields.io/aur/votes/mshr-git.svg)](https://aur.archlinux.org/packages/mshr-git)

  because `tetgen` is not available without tweaks.

<!-- ## Out of date packages -->

<!-- https://github.com/rixx/pkgbuilds/blob/master/README.md?plain=1
## Resources

 - [general AUR article](https://wiki.archlinux.org/index.php/Arch_User_Repository)
 - [Creating packages](https://wiki.archlinux.org/index.php/Creating_packages)
 - [Arch packaging standards](https://wiki.archlinux.org/index.php/Arch_packaging_standards)
 - [PKGBUILD](https://wiki.archlinux.org/index.php/PKGBUILD)
 - [makepkg](https://wiki.archlinux.org/index.php/Makepkg)
 - [.SRCINFO](https://wiki.archlinux.org/index.php/.SRCINFO)
 - [VCS package guidelines](https://wiki.archlinux.org/index.php/VCS_package_guidelines)
 - [Namcap](https://wiki.archlinux.org/index.php/Namcap)

-->

## Many thanks

[![#CCOSS - Taller de contribución a Arch Linux](http://img.youtube.com/vi/a4KpbdGiwtk/0.jpg)](https://yewtu.be/embed/a4KpbdGiwtk "#CCOSS - Taller de contribución a Arch Linux")

Powered by [`archlinux-docker`](https://gitlab.archlinux.org/archlinux/archlinux-docker), [`nvchecker`](https://github.com/lilydjwg/nvchecker), [`urlwatch`](https://github.com/thp/urlwatch), [`newreleases.io`](https://newreleases.io), [`gitpod-io`](https://github.com/gitpod-io), [$`\KaTeX`$](https://katex.org).
