#!/usr/bin/env bash

# shellcheck disable=SC2046,SC2155,SC2116

set -e -u

function check_aur_packages() {
  local URL=https://gitlab.com/dune-archiso/testing/aur/aur/-/raw/main/packages.x86_64
  local lista=$(curl -s $URL | tr '\n' ' ')
  aur-out-of-date -pkg $(echo "$lista")
}

function check_copackages() {
  local URL=https://gitlab.com/dune-archiso/testing/aur/aur/-/raw/main/copackages.x86_64
  local lista=$(curl -s $URL | tr '\n' ' ')
  aur-out-of-date -pkg $(echo "$lista")
}

function check_dependencies_packages() {
  local URL=https://gitlab.com/dune-archiso/testing/aur/aur/-/raw/main/dependencies.x86_64
  local lista=$(curl -s $URL | tr '\n' ' ')
  aur-out-of-date -pkg $(echo "$lista")
}

function nvchecker_packages() {
  nvchecker -c config_file.toml
  clear # TODO: Debug mode
  cat aur-new.txt | jq
  nvtake -c config_file.toml
  nvcmp -c config_file.toml
}

if [[ $EUID -eq 33333 ]]; then
  echo "Hola ${USER}"
  #sed -i 's/^keyfile = "apikey.cript"/keyfile = "keyfile"/' "${THEIA_WORKSPACE_ROOT}/config_file.toml"
fi

if hash aur-out-of-date 2>/dev/null; then
  # check_aur_packages
  # check_copackages
  check_dependencies_packages
else
  echo "Run $(yay -Syyu aur-out-of-date)."
fi

# if hash nvchecker 2>/dev/null; then
#   nvchecker_packages
# else
#   echo "Run $(sudo pacman -Syyuq nvchecker)."
# fi
