[__config__]
oldver = "aur-old.txt"
newver = "aur-new.txt"
max_concurrency = 20
http_timeout = 8000
keyfile = "apikey.crypt"
#keyfile = "keyfile"

#################################
# Recompression utilities for .zip .png .mng and .gz files using the 7-zip algorithm
#################################

[advancecomp]
source = "github"
github = "amadvance/advancecomp"
use_max_tag = "true"

[advancecomp-alpine]
source = "repology"
repology = "advancecomp"
repo = "alpine_edge"

# [advancecomp-apt]
# source = "apt"
# srcpkg = "advancecomp"
# mirror = "mirror.math.princeton.edu/pub/debian"
# suite = "sid"

[advancecomp-freebsd]
source = "repology"
repology = "advancecomp"
repo = "freebsd"

[advancecomp-nix]
source = "repology"
repology = "advancecomp"
repo = "nix_unstable"

#################################
# Live reload for Go apps
#################################

[air]
source = "aur"
strip_release = "true"

[air-bin]
source = "aur"
strip_release = "true"

[air-bin-github]
source = "github"
github = "cosmtrek/air"
use_max_tag = "true"

[air-mac]
source = "repology"
repology = "air-live-reload"
repo = "macports"

[air-nix]
source = "repology"
repology = "air-live-reload"
repo = "nix_unstable"

#################################
# Adaptive finite element toolbox
#################################

[alberta]
source = "aur"
strip_release = "true"

[alberta-apt]
source = "apt"
srcpkg = "alberta"
mirror = "mirror.math.princeton.edu/pub/debian"
suite = "sid"

[alberta-gitlab]
source = "gitlab"
gitlab = "ians-nmh/alberta/alberta3"
branch = "master"
host = "gitlab.mathematik.uni-stuttgart.de"

[alberta-ubuntu]
source = "ubuntupkg"
ubuntupkg = "alberta"
suite = "jammy"

#################################
# A flexible, lightweight multi-language test reporting tool
#################################

[allure-commandline]
source = "github"
github = "allure-framework/allure2"
use_max_tag = "true"

#################################
# MIME encoded email pack alteration tool
#################################

[altermime-alpine]
source = "repology"
repology = "altermime"
repo = "alpine_edge"

# TODO: False positive
[altermime-web]
source = "regex"
encoding = "utf-8"
url = "https://pldaniels.com/altermime/"
regex = "altermime-([0-9].[0-9].[0-9][0-9]+).tar.gz"

#################################
# Adaptive Message Passing Interface, OpenMPI backend
#################################

[charm]
source = "github"
github = "UIUC-PPL/charm"
use_max_tag = "true"

[charm-spack]
source = "repology"
repology = "charmpp"
repo = "spack"

[charm-web]
source = "regex"
encoding = "utf-8"
url = "http://charm.cs.illinois.edu/distrib/"
regex = "charm-([0-9].[0-9].[0-9]+).tar.gz"

#################################
# AMPL Solver Library
#################################

[ampl-asl-web]
source = "regex"
encoding = "utf-8"
url = "https://raw.githubusercontent.com/ampl/asl/master/src/solvers/asldate.c"
regex = "long ASLdate_ASL = (202[0-9][0-9][0-9][0-9][0-9]+);"

#################################
# An open-source library for mathematical programming
#################################

[ampl-mp]
source = "github"
github = "ampl/mp"
use_latest_release = "true"

[ampl-mp-homebrew]
source = "repology"
repology = "ampl-mp"
repo = "homebrew"

#################################
# Arpack++ with patches
#################################

[arpackpp]
source = "github"
github = "m-reuter/arpackpp"
use_max_tag = "true"

[arpackpp-apt]
source = "apt"
srcpkg = "arpack++"
mirror = "mirror.math.princeton.edu/pub/debian"
suite = "sid"

[arpackpp-ubuntu]
source = "repology"
repology = "arpack++"
repo = "ubuntu_22_04"

#################################
# Find broken links, missing images, etc within your HTML
#################################

[broken-link-checker-npm]
source = "npm"
npm = "broken-link-checker"

#################################
# CMake wrapper CLI tool
#################################

[cmakew]
source = "github"
github = "thombashi/cmakew"
use_max_tag = "true"

[cmakew-pypi]
source = "pypi"
pypi = "cmakew"

#################################
# COIN-OR solver for mixed-integer linear programs (MILPs)
#################################

[coin-or-symphony]
source = "github"
github = "coin-or/SYMPHONY"
use_latest_release = "true"

[coin-or-symphony-apt]
source = "apt"
srcpkg = "coinor-symphony"
mirror = "mirror.math.princeton.edu/pub/debian"
suite = "sid"

[coin-or-symphony-fedora]
source = "repology"
repology = "coin-or-symphony"
repo = "fedora_rawhide"

[coin-or-symphony-freebsd]
source = "repology"
repology = "coin-or-symphony"
repo = "freebsd"

[coin-or-symphony-gentoo]
source = "repology"
repology = "coin-or-symphony"
repo = "gentoo"

[coin-or-symphony-ubuntu]
source = "repology"
repology = "coin-or-symphony"
repo = "ubuntu_22_04"

#################################
# A dynamic array library (similar to python-numpy but written in C++)
#################################

[dynd]
source = "github"
github = "libdynd/libdynd"
use_max_tag = true

[dynd-gentoo]
source = "repology"
repology = "libdynd"
repo = "gentoo"

[dynd-macports]
source = "repology"
repology = "libdynd"
repo = "macports"

[dynd-nix_unstable]
source = "repology"
repology = "libdynd"
repo = "nix_unstable"

#################################
# Terminal/console based exam, test, or quiz tool for educators and learners
#################################

[exam-terminal]
source = "github"
github = "ismet55555/exam-terminal"

[exam-terminal-pypi]
source = "pypi"
pypi = "exam-terminal"

[exam-terminal-web]
source = "regex"
encoding = "utf-8"
url = "https://raw.githubusercontent.com/ismet55555/exam-terminal/master/setup.py"
regex = "__version__ = \"([0-9].[0-9].[0-9]+)\""

#################################
# Halcyon 3d virtual reality world simulator
#################################

[halcyon]
source = "github"
github = "HalcyonGrid/halcyon"

#################################
# A language for fast and portable data-parallel computation
#################################

[halide-github]
source = "github"
github = "halide/Halide"

[halide-apt]
source = "apt"
srcpkg = "halide"
mirror = "mirror.math.princeton.edu/pub/debian"
suite = "sid"

[halide]
source = "aur"

[halide-freebsd]
source = "repology"
repology = "halide"
repo = "freebsd"

[halide-homebrew]
source = "repology"
repology = "halide"
repo = "homebrew"

[halide-ubuntu]
source = "repology"
repology = "halide"
repo = "ubuntu_22_04"

[halide-vcpkg]
source = "repology"
repology = "halide"
repo = "vcpkg"

#################################
# Mercurial interactive Qt based history viewer
#################################

[hgview-altsisyphus]
source = "repology"
repology = "hgview"
repo = "altsisyphus"

[hgview-fedora]
source = "repology"
repology = "hgview"
repo = "fedora_rawhide"

# FIXME: 
#[hgview-web]
#source = "regex"
#encoding = "utf-8"
#url = "http://download.logilab.org/pub/hgview/"
#regex = "hgview-([0-9].[0-9].[0-9]+).tar.gz"

#################################
# Parallel solvers for sparse linear systems featuring multigrid methods
#################################

[hypre]
source = "github"
github = "hypre-space/hypre"
use_max_tag = "true"

[hypre-apt]
source = "apt"
srcpkg = "hypre"
mirror = "mirror.math.princeton.edu/pub/debian"
suite = "sid"

[hypre-freebsd]
source = "repology"
repology = "hypre"
repo = "freebsd"

[hypre-homebrew]
source = "repology"
repology = "hypre"
repo = "homebrew"

[hypre-msys2_mingw]
source = "repology"
repology = "hypre"
repo = "msys2_mingw"

[hypre-spack]
source = "repology"
repology = "hypre"
repo = "spack"

[hypre-ubuntu]
source = "repology"
repology = "hypre"
repo = "ubuntu_22_04"

[hypre-vcpkg]
source = "repology"
repology = "hypre"
repo = "vcpkg"

#################################
# PNG/JPEG optimization app
#################################

[imagine]
source = "github"
github = "meowtec/imagine"
use_latest_release = "true"

#################################
# A Jupyter/IPython kernel for gnuplot
#################################

[jupyter-gnuplot_kernel]
source = "github"
github = "has2k1/gnuplot_kernel"
use_max_tag = "true"

#################################
# Mesh generation component of FEniCS (git version)
#################################

[mshr]
source = "bitbucket"
bitbucket = "fenics-project/mshr"
use_max_tag = "true"

[mshr-opensuse]
source = "repology"
repology = "mshr"
repo = "opensuse_science_tumbleweed"

[mshr-ubuntu]
source = "repology"
repology = "mshr"
repo = "ubuntu_22_04"

#################################
# First-come-first-serve single-fire HTTP/HTTPS server
#################################

[oneshot]
source = "aur"

[oneshot-freebsd]
source = "repology"
repology = "oneshot"
repo = "freebsd"

[oneshot-github]
source = "github"
github = "raphaelreyna/oneshot"
use_max_tag = "true"

[oneshot-nix_unstable]
source = "repology"
repology = "oneshot"
repo = "nix_unstable"

#################################
# A 3D application server used to create a virtual environment or world
#################################

[opensim-web]
source = "regex"
encoding = "utf-8"
url = "http://opensimulator.org/dist/"
regex = "opensim-([0-9].[0-9].[0-9].[0-9]+)-source.tar.gz"

#################################
# OSGrid's distribution of OpenSimulator, preconfigured to connect with OSGrid
#################################

# FIXME:
# [opensim-osgrid-web]
# source = "regex"
# encoding = "utf-8"
# url = "https://danbanner.onikenkon.com/osgrid/"
# regex = "osgrid-opensim-([0-9][0-9][0-9][0-9]202[1-9].v[0-9].[0-9].[0-9].*.+).zip"

#################################
# A font to typeset maths in Xe(La)TeX and Lua(La)TeX by Apostolos Syropoulos
#################################

# FIXME:
# [otf-asana-math-web]
# source = "regex"
# encoding = "utf-8"
# url = "http://mirrors.ctan.org/systems/texlive/tlnet/archive/"
# regex = "Asana-Math.tar.xz"

#################################
# A font family converted from D. Knuth's Computer Modern metafont
#################################

[otf-cm-unicode-web]
source = "regex"
encoding = "utf-8"
url = "http://mirrors.ctan.org/systems/texlive/tlnet/archive/"
regex = "cm-unicode.tar.xz"

#################################
# An open type mathematical font from the Greek Font Society
#################################

# FIXME:
# [otf-gfs-neohellenic-math-web]
# source = "regex"
# encoding = "utf-8"
# url = "http://www.greekfontsociety-gfs.gr/_assets/fonts/"
# regex = "GFS_NeoHellenic_Math.zip"

#################################
# Awesome multilingual OCR toolkits based on PaddlePaddle
#################################

[paddleocr]
source = "github"
github = "PaddlePaddle/paddleocr"
use_latest_release = "true"

#################################
# Machine Learning Framework from Industrial Practice
#################################

[python-paddlepaddle]
source = "aur"
strip_release = "true"

[paddlepaddle-pypi]
source = "pypi"
pypi = "paddlepaddle"

#################################
# Creating PDFs from images or scanner made easy
#################################

[pdfquirk]
source = "github"
github = "dragotin/pdfquirk"
use_max_tag = "true"

#################################
# * An easy-to-use, free of charge, photo server for your home network
#################################

# FIXME:
# [picapport-web]
# source = "regex"
# encoding = "utf-8"
# url = "https://www.picapport.de/prepare_download.php"
# regex = "?dlfile=([0-9][0-9]-[0-9]-[0-9][0-9]+)/picapport.jar"

#################################
# A multi format lossless image optimizer that uses external tools
#################################

[picopt]
source = "github"
github = "ajslater/picopt"
use_max_tag = "true"

[picopt-pypi]
source = "pypi"
pypi = "picopt"

#################################
# Piecewise linear bijections between triangulated surfaces
#################################

[psurface]
source = "github"
github = "psurface/psurface"
use_max_tag = "true"

#################################
# Pure Python implementation of d-dimensional AABB tree
#################################

[python-aabbtree]
source = "github"
github = "kip-hart/AABBTree"
use_max_tag = "true"

[python-aabbtree-pypi]
source = "pypi"
pypi = "aabbtree"

#################################
# Accurate sums and dot products for Python
#################################

[python-accupy]
source = "github"
github = "nschloe/accupy"
use_max_tag = "true"

[python-accupy-nix_unstable]
source = "repology"
repology = "python:accupy"
repo = "nix_unstable"

[python-accupy-pypi]
source = "pypi"
pypi = "accupy"

#################################
# Adaptive mesh generation and refinement
#################################

[python-adaptmesh]
source = "github"
github = "kinnala/adaptmesh"
use_max_tag = "true"

[python-adaptmesh-pypi]
source = "pypi"
pypi = "adaptmesh"

#################################
# A library for Automatic Differentation in Python
#################################

[python-algopy-freebsd]
source = "repology"
repology = "python:algopy"
repo = "freebsd"

[python-algopy-macports]
source = "repology"
repology = "python:algopy"
repo = "macports"

[python-algopy-pypi]
source = "pypi"
pypi = "algopy"

#################################
# Common module for integrate allure with python-based frameworks
#################################

[python-allure-commons-pypi]
source = "pypi"
pypi = "allure-python-commons"

[python-allure-commons-nix_unstable]
source = "repology"
repology = "python:accupy"
repo = "nix_unstable"

[python-dynd-macports]
source = "repology"
repology = "python:dynd"
repo = "macports"

[python-dynd-nix_unstable]
source = "repology"
repology = "python:dynd"
repo = "nix_unstable"
