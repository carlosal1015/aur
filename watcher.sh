#!/usr/bin/env bash

set -e -u

function watch_url_packages() {
  urlwatch --help
}

if hash nvchecker 2>/dev/null; then
  watch_url_packages
else
  echo "Run $(sudo pacman -Syyuq urlwatch)."
fi
